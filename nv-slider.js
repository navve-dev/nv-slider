/**
 * Adds horizontal slide behavior to html elements.
 * @author Lanza
 */

(function (doc, viewport) {

    const Lanzapages = function (
        area,
        track,
        paginationContainer,
        nextButton,
        prevButton,
        viewport,
        vertical,
        pageActiveClass,
        buttonActiveClass
    ) {
        this.track = {
            element: track,
            size: 0
        };
        this.area = {
            element: area,
            size: 0
        };
        this.vertical = !!vertical;
        this.viewport = {
            element: viewport,
            fn: null
        };
        this.pages = {
            element: paginationContainer,
            current: 0,
            coords: [0],
            activeClass: pageActiveClass
        };
        this.navigation = {
            next: {
                enabled: false,
                element: nextButton,
                fn: null,
            },
            prev: {
                enabled: false,
                element: prevButton,
                fn: null
            },
            activeClass: buttonActiveClass,
            swipe: {
                fnStart: null,
                fnEnd: null
            }
        };
    };

    const p = Lanzapages.prototype;

    p.updatePagesInterface = function () {
        const el = this.pages.element;
        const nPages = this.pages.coords.length;
        const activeClass = this.pages.activeClass || 'active';
        if(el) {
            if(el.childElementCount !== nPages) {
                el.innerHTML = '';
                if(nPages > 1) {
                    for(let a = 0; a < nPages; a++) {
                        let span = this.viewport.element.document.createElement('span');
                        el.appendChild(span);
                        span.addEventListener('click', this.gotoPage.bind(this, a));
                    }
                }
            }
            let pageEl = el.querySelector('span.' + activeClass);
            if(pageEl) {
                pageEl.classList.remove(activeClass);
            }
            pageEl = el.querySelector('span:nth-child(' + (this.pages.current + 1) + ')');
            if(pageEl) {
                pageEl.classList.add(activeClass);
            }
        }
    };

    p.updateInterface = function () {
        const coord = this.pages.coords[this.pages.current];
        const style = this.vertical ? `translate(0, -${coord}px)` : `translate(-${coord}px, 0)`;
        const next = this.navigation.next;
        const prev = this.navigation.prev;
        const btActiveClass = this.navigation.activeClass || 'active';
        this.track.element.style.transform = style;
        if(next.element) {
            next.element.classList.toggle(btActiveClass, next.enabled);
        }
        if(prev.element) {
            prev.element.classList.toggle(btActiveClass, prev.enabled);
        }
        this.updatePagesInterface();
    };

    p.updateNavigation = function () {
        const p = this.pages;
        this.navigation.prev.enabled = p.current > 0;
        this.navigation.next.enabled = p.current < p.coords.length - 1;
    };

    p.definePagination = function () {
        const trackRect = this.track.element.getBoundingClientRect();
        const areaRect = this.area.element.getBoundingClientRect();
        let dimension, posProp;
        if(this.vertical) {
            dimension = 'height';
            posProp = 'offsetTop';
        } else {
            dimension = 'width';
            posProp = 'offsetLeft';
        }
        const trackSize = trackRect[dimension] + this.track.element[posProp];
        const areaSize = areaRect[dimension];
        this.track.size = trackSize;
        this.area.size = areaSize;
        this.pages.coords = [0];
        if(trackSize > areaSize && this.track.element.childElementCount) {
            let elementsSize = 0;
            let prevElRect = null;
            let prevEl = null;
            let prevCoord = 0;
            for(let a = 0, n = this.track.element.childElementCount; a < n; a++) {
                let el = this.track.element.children[a];
                let elRect = el.getBoundingClientRect();
                elementsSize += elRect[dimension];
                if(prevElRect) {
                    elementsSize += el[posProp] - prevEl[posProp] - prevElRect[dimension];
                }
                if(elementsSize > areaSize) {
                    let newCoord = prevCoord + elementsSize - elRect[dimension];
                    this.pages.coords.push(newCoord);
                    elementsSize = elRect[dimension];
                    prevCoord = newCoord;
                }
                prevElRect = elRect;
                prevEl = el;
            }
        }
    };

    p.resetScroll = function () {
        this.pages.current = 0;
        this.updateNavigation();
    };

    p.emitScrollEvent = function () {
        const evt = new Event('lanzascroll', { bubbles: true });
        this.area.element.dispatchEvent(evt);
    };

    p.gotoPage = function (pageIndex) {
        if(pageIndex !== this.pages.current) {
            this.pages.current = pageIndex;
            this.updateNavigation();
            this.updateInterface();
            this.emitScrollEvent();
        }
    };

    p.nextPage = function () {
        if(this.navigation.next.enabled) {
            this.gotoPage(Math.min(this.pages.current + 1, this.pages.coords.length - 1));
        }
    };

    p.prevPage = function () {
        if(this.navigation.prev.enabled) {
            this.gotoPage(Math.max(this.pages.current - 1, 0));
        }
    };

    p.enableSwipe = function () {
        let pstart = 0, pend = 0;
        const eventProp = this.vertical ? 'screenY' : 'screenX';

        this.navigation.swipe.fnStart = evt => {
            pstart = evt.changedTouches[0][eventProp];
        };

        this.navigation.swipe.fnEnd = evt => {
            pend = evt.changedTouches[0][eventProp];
            const diff = pend - pstart;
            if(diff > 10) {
                this.prevPage();
            } else if(diff < -10) {
                this.nextPage();
            }
        };

        this.area.element.addEventListener('touchstart', this.navigation.swipe.fnStart);
        this.area.element.addEventListener('touchend', this.navigation.swipe.fnEnd);
    };

    p.disableSwipe = function () {
        if(this.navigation.swipe.fnStart) {
            this.area.element.removeEventListener('touchstart', this.navigation.swipe.fnStart);
            this.navigation.swipe.fnStart = null;
        }
        if(this.navigation.swipe.fnEnd) {
            this.area.element.removeEventListener('touchend', this.navigation.swipe.fnEnd);
            this.navigation.swipe.fnEnd = null;
        }
    };

    p.init = function () {
        this.definePagination();
        this.resetScroll();
        if(this.navigation.next.element) {
            this.navigation.next.fn = this.nextPage.bind(this);
            this.navigation.next.element.addEventListener('click', this.navigation.next.fn);
        }
        if(this.navigation.prev.element) {
            this.navigation.prev.fn = this.prevPage.bind(this);
            this.navigation.prev.element.addEventListener('click', this.navigation.prev.fn);
        }
        this.enableSwipe();
        this.updateInterface();
    };

    p.disable = function () {
        if(this.navigation.next.fn) {
            this.navigation.next.element.removeEventListener('click', this.navigation.next.fn);
            this.navigation.next.fn = null;
        }
        if(this.navigation.prev.fn) {
            this.navigation.prev.element.removeEventListener('click', this.navigation.prev.fn);
            this.navigation.prev.fn = null;
        }
        this.disableSwipe();
    };

    p.resize = function () {
        const prevCoords = this.pages.coords;
        this.definePagination();
        const changed = this.pages.coords.length !== prevCoords.length ||
            this.pages.coords.some((coord, index) => {
                return coord !== prevCoords[index];
            });
        if(changed) {
            this.resetScroll();
            this.updateInterface();
        }
    };

    p.destroy = function () {
        this.disable();
        this.viewport.element.removeEventListener('resize', this.viewport.fn);
    };


    const NvSlider = {

        sliders: [],

        binded: {
            resizeSliders: null,
            setup: null
        },

        animationFrame: null,

        createSlider: function (element) {
            const area = element.querySelector('.nv-slider-area');
            const track = element.querySelector('.nv-slider-track');
            const btPrev = element.querySelector('.nv-slider-prev-button') || doc.createElement('div');
            const btNext = element.querySelector('.nv-slider-next-button') || doc.createElement('div');
            const pagination = element.querySelector('.nv-slider-pagination') || doc.createElement('div');
            const vertical = element.classList.contains('nv-slider-vertical');
            if(area && track) {
                return new Lanzapages(area, track, pagination, btNext, btPrev, viewport, vertical, 'nv-page-active', 'nv-enabled');
            } else {
                return null;
            }
        },

        resizeSliders: function () {
            viewport.cancelAnimationFrame(this.animationFrame);
            this.animationFrame = viewport.requestAnimationFrame(() => {
                this.sliders.forEach(slider => {
                    slider.resize();
                });
            });
        },

        setup: function () {
            const els = doc.querySelectorAll('.nv-slider');
            for(let a = 0, n = els.length; a < n; a++) {
                let slider = this.createSlider(els[a]);
                if(slider) {
                    this.sliders.push(slider);
                    slider.init();
                }
            }
            viewport.addEventListener('resize', this.binded.resizeSliders);
        },

        destroy: function () {
            this.sliders.forEach(slider => {
                slider.destroy();
            });
            viewport.removeEventListener('load', this.binded.setup);
            viewport.removeEventListener('resize', this.binded.resizeSliders);
        },

        create: function () {
            this.binded.setup = this.setup.bind(this);
            this.binded.resizeSliders = this.resizeSliders.bind(this);
            if(doc.readyState !== 'complete') {
                viewport.addEventListener('load', this.binded.setup);
            } else {
                this.setup();
            }
        }

    };

    NvSlider.create();

})(document, window);