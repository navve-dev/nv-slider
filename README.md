# NvSlider

Adds horizontal slide-show behavior to html elements. Useful for carousel-like components.

## How to use

Just add the script in your page. It will search for any element with `nv-slider` class and convert it to a horizontal slider.

There must be elements with the following css classes within your `nv-slider`:

* `nv-slider-area`: Required. The canvas area of the slider.
* `nv-slider-track`: Required. The element that contains the slides and will be moved by NvSlider.
* `nv-slider-prev-button`: Optional. The button to previous page.
* `nv-slider-next-button`: Optional. The button to next page.

### CSS

NvSlider does not set any style besides _translate_ in `nv-slider-track`. Yours elements must be styled accordingly.

## Author

Coded by [Lanza](mailto:fabio.lanzarin@portotech.org).